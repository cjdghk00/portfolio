﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text goldDisplayer;
    public Text goldPerClickDisplayer;
    

    void Update()
    {
        goldDisplayer.text = "골드 " + DataController.GetInstance().GetGold();
        goldPerClickDisplayer.text = "클릭당골드 " + DataController.GetInstance().GetGoldPerClick();
    }

}
