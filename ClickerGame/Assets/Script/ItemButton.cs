﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ItemButton : MonoBehaviour
{
    public Text itemDisplayer;
    public string itemName;

    public int goldPerSec;
    public int startGoldPerSec = 1;

    public int currentCost;
    public int startCurrentCost = 1;

    public int level;
    public float upgradePow = 1.07f;
    public float costPow = 3.14f;

    public bool isPurchase = false;

    void Start()
    {
        DataController.GetInstance().LoadItemButton(this);
        StartCoroutine("AddGoldLoop");
        UpdateItem();
        UpdateUI();
    }


    public void PurchaseItem()
    {
        if (DataController.GetInstance().GetGold() >= currentCost)
        {
            isPurchase = true;
            DataController.GetInstance().SubGold(currentCost);
            //level = level + 1;
            level++;
            UpdateItem();
            UpdateUI();
            DataController.GetInstance().SaveItemButton(this);
        }
    }

    IEnumerator AddGoldLoop() //IEnumerator 반복함수!
    {
        while (true)
        {
            if (isPurchase == true)
            {
                DataController.GetInstance().AddGold(goldPerSec);
            }
            yield return new WaitForSeconds(1.0f); //시간을 정해줘야 함.

        }
    }

    public void UpdateItem()
    {
        goldPerSec = goldPerSec + startGoldPerSec * (int) upgradePow * level;
        currentCost = startCurrentCost * (int)costPow * level;
    }

    public void UpdateUI()
    {
        itemDisplayer.text = "아이템 이름: " + itemName + "\n레벨: " + level + "\n업그레이드 비용: " + currentCost + "\n초당 획득 골드: " + goldPerSec + "\n구매여부: " + isPurchase;
    }

}
