﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour
{

    public Text upgradeDisplayer;
    public string upgradeName;

    public int goldByUpgrade;
    public int startGoldByUpgrade = 1;

    public int currentCost;
    public int startCurrentCost = 1;

    public int level = 1;
    public float upgradePow = 1.07f;
    public float costPow = 3.14f;

    void Start()
    {
        DataController.GetInstance().LoadUpgradeButton(this);
        UpdateUI();
    }

    public void PurchaseUpgrade()
    {
        if (DataController.GetInstance().GetGold() >= currentCost)
        {
            DataController.GetInstance().SubGold(currentCost);
            level = level + 1;
            DataController.GetInstance().AddGoldPerClick(goldByUpgrade);
            UpdateUpgrade();
            UpdateUI();
            DataController.GetInstance().SaveUpgradeButton(this);
        }
    }

    public void UpdateUpgrade()
    {
        goldByUpgrade = startGoldByUpgrade * (int)upgradePow * level;
        currentCost = startCurrentCost * (int)costPow * level;
    }

    public void UpdateUI()
    {
        upgradeDisplayer.text = upgradeName + "\n비용 : " + currentCost + "\n레벨 : " + level + "\n다음 업그레이드 비용 : " + goldByUpgrade;
    }

}
