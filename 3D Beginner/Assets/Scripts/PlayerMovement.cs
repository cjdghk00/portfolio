﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f; // 캐릭터 좌우로 움직이는 속도

    Animator m_Animator; 
    Rigidbody m_Rigidbody;
    Vector3 m_Movement; //이동하는 함수

    Quaternion m_Rotation = Quaternion.identity; //이동하는 회전 함수
    AudioSource m_AudioSource;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal"); //좌,우
        float vertical = Input.GetAxis("Vertical"); //상,하

        m_Movement.Set(horizontal, 0f, vertical); //위에 두개를 계산해서 벡터값으로 해줌.
        m_Movement.Normalize();

        bool hasHorizontallnput = !Mathf.Approximately(horizontal, 0f); // 애니메이션 관련
        bool hasVerticallnput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontallnput || hasVerticallnput;
        m_Animator.SetBool("IsWalking", isWalking);
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward); // 회전처리

    }
    private void OnAnimatorMove() // 애니메이션 재생시 물리를 밀어주는 것
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}
